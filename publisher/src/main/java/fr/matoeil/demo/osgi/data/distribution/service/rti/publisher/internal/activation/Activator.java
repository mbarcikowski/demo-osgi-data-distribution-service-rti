package fr.matoeil.demo.osgi.data.distribution.service.rti.publisher.internal.activation;

import fr.matoeil.demo.osgi.data.distribution.service.rti.api.Deal;
import fr.matoeil.demo.osgi.data.distribution.service.rti.publisher.internal.RTIPublisher;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class Activator
    implements BundleActivator
{

    private final ScheduledExecutorService scheduler_ = Executors.newScheduledThreadPool( 1 );

    private final RTIPublisher rtiPublisher_ = new RTIPublisher();

    private static String hostName;

    static
    {

        InetAddress addr = null;
        try
        {
            addr = InetAddress.getLocalHost();
            hostName = addr.getHostName() + "::";
        }
        catch ( UnknownHostException e )
        {
            hostName = "unknown::";

        }
    }

    final Runnable timeRunnable_ = new Runnable()
    {
        public void run()
        {
            String message = hostName + "current time : " + Long.toString( System.currentTimeMillis() );
            System.out.println( "try publish " + message );
            rtiPublisher_.publish( message );
        }
    };

    final Runnable dealRunnable_ = new Runnable()
    {
        public void run()
        {

            final String id = hostName + "dealid";
            final int value = (int) ( Math.random() * Integer.MAX_VALUE );
            Deal deal = new Deal( id, value );
            System.out.println( "try publish " + deal );
            rtiPublisher_.publish( deal );
        }
    };

    @Override
    public void start( final BundleContext context )
        throws Exception
    {
        rtiPublisher_.start();
        scheduler_.scheduleAtFixedRate( timeRunnable_, 10, 10, TimeUnit.SECONDS );
        scheduler_.scheduleAtFixedRate( dealRunnable_, 10, 10, TimeUnit.SECONDS );

    }

    @Override
    public void stop( final BundleContext context )
        throws Exception
    {
        scheduler_.shutdown();
        rtiPublisher_.stop();

    }


}
