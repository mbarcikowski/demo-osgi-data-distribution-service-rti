package fr.matoeil.demo.osgi.data.distribution.service.rti.publisher.internal;

import com.rti.dds.domain.DomainParticipant;
import com.rti.dds.domain.DomainParticipantFactory;
import com.rti.dds.infrastructure.InstanceHandle_t;
import com.rti.dds.infrastructure.StatusKind;
import com.rti.dds.publication.Publisher;
import com.rti.dds.topic.Topic;
import com.rti.dds.type.builtin.StringDataWriter;
import com.rti.dds.type.builtin.StringTypeSupport;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.Deal;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.message.DealMessage;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.message.DealMessageDataWriter;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.message.DealMessageTypeSupport;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class RTIPublisher
{
    private static final int DOMAIN_ID = 0;

    private DomainParticipant participant_;

    private Topic timeTopic_;

    private StringDataWriter timeDataWriter_;

    private Topic dealTopic_;

    private DealMessageDataWriter dealDataWriter_;

    public void start()
    {

        DomainParticipantFactory domainParticipantFactory = DomainParticipantFactory.get_instance();

        //join domain
        participant_ =
            domainParticipantFactory.create_participant( DOMAIN_ID, DomainParticipantFactory.PARTICIPANT_QOS_DEFAULT,
                                                         null, StatusKind.STATUS_MASK_NONE );

        //create time topic
        timeTopic_ =
            participant_.create_topic( "time", StringTypeSupport.get_type_name(), DomainParticipant.TOPIC_QOS_DEFAULT,
                                       null, StatusKind.STATUS_MASK_NONE );

        // create time datawriter
        timeDataWriter_ =
            (StringDataWriter) participant_.create_datawriter( timeTopic_, Publisher.DATAWRITER_QOS_DEFAULT, null,
                                                               StatusKind.STATUS_MASK_NONE );

        //register deal type
        DealMessageTypeSupport.register_type( participant_, DealMessageTypeSupport.get_type_name() );

        //create deal topic
        dealTopic_ = participant_.create_topic( "deal", DealMessageTypeSupport.get_type_name(),
                                                DomainParticipant.TOPIC_QOS_DEFAULT, null,
                                                StatusKind.STATUS_MASK_NONE );

        // create deal datawriter
        dealDataWriter_ =
            (DealMessageDataWriter) participant_.create_datawriter( dealTopic_, Publisher.DATAWRITER_QOS_DEFAULT, null,
                                                                    StatusKind.STATUS_MASK_NONE );
    }

    public void stop()
    {
        participant_.delete_datawriter( timeDataWriter_ );
        participant_.delete_topic( timeTopic_ );
        participant_.delete_datawriter( dealDataWriter_ );
        participant_.delete_topic( dealTopic_ );
        participant_.unregister_type( DealMessageTypeSupport.get_type_name() );
        DomainParticipantFactory domainParticipantFactory = DomainParticipantFactory.get_instance();
        domainParticipantFactory.delete_participant( participant_ );
    }

    public void publish( final String aMessage )
    {
        timeDataWriter_.write( aMessage, InstanceHandle_t.HANDLE_NIL );
    }

    public void publish( final Deal aDeal )
    {
        final DealMessage dealMessage = new DealMessage();
        dealMessage.id = aDeal.getId();
        dealMessage.value = aDeal.getValue();
        dealDataWriter_.write( dealMessage, InstanceHandle_t.HANDLE_NIL );
    }
}
