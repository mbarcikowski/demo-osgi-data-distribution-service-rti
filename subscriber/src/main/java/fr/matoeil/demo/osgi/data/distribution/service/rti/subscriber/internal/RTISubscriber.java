package fr.matoeil.demo.osgi.data.distribution.service.rti.subscriber.internal;

import com.rti.dds.domain.DomainParticipant;
import com.rti.dds.domain.DomainParticipantFactory;
import com.rti.dds.infrastructure.StatusKind;
import com.rti.dds.subscription.DataReader;
import com.rti.dds.subscription.DataReaderAdapter;
import com.rti.dds.subscription.SampleInfo;
import com.rti.dds.subscription.Subscriber;
import com.rti.dds.topic.Topic;
import com.rti.dds.type.builtin.StringDataReader;
import com.rti.dds.type.builtin.StringTypeSupport;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.Deal;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.message.DealMessage;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.message.DealMessageDataReader;
import fr.matoeil.demo.osgi.data.distribution.service.rti.api.message.DealMessageTypeSupport;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class RTISubscriber
{
    private static final int DOMAIN_ID = 0;

    private DomainParticipant participant_;

    private Topic timeTopic_;

    private DataReader timeDataReader_;

    private Topic dealTopic_;

    private DataReader dealDataReader_;

    public void start()
    {
        //join domain
        DomainParticipantFactory domainParticipantFactory = DomainParticipantFactory.get_instance();
        participant_ =
            domainParticipantFactory.create_participant( DOMAIN_ID, DomainParticipantFactory.PARTICIPANT_QOS_DEFAULT,
                                                         null, StatusKind.STATUS_MASK_NONE );

        //create time topic
        timeTopic_ =
            participant_.create_topic( "time", StringTypeSupport.get_type_name(), DomainParticipant.TOPIC_QOS_DEFAULT,
                                       null, StatusKind.STATUS_MASK_NONE );

        // create time dataReader
        timeDataReader_ =
            participant_.create_datareader( timeTopic_, Subscriber.DATAREADER_QOS_DEFAULT, new TimeDataReaderAdapter(),
                                            StatusKind.DATA_AVAILABLE_STATUS );

        //register deal type
        DealMessageTypeSupport.register_type( participant_, DealMessageTypeSupport.get_type_name() );

        //create deal topic
        dealTopic_ = participant_.create_topic( "deal", DealMessageTypeSupport.get_type_name(),
                                                DomainParticipant.TOPIC_QOS_DEFAULT, null,
                                                StatusKind.STATUS_MASK_NONE );

        // create deal dataReader
        dealDataReader_ =
            participant_.create_datareader( dealTopic_, Subscriber.DATAREADER_QOS_DEFAULT, new DealDataReaderAdapter(),
                                            StatusKind.DATA_AVAILABLE_STATUS );

    }

    public void stop()
    {
        participant_.delete_datareader( timeDataReader_ );
        participant_.delete_topic( timeTopic_ );
        participant_.delete_datareader( dealDataReader_ );
        participant_.delete_topic( dealTopic_ );
        participant_.unregister_type( DealMessageTypeSupport.get_type_name() );
        DomainParticipantFactory domainParticipantFactory = DomainParticipantFactory.get_instance();
        domainParticipantFactory.delete_participant( participant_ );
    }

    private static class TimeDataReaderAdapter
        extends DataReaderAdapter
    {
        @Override
        public void on_data_available( final DataReader aDataReader )
        {
            StringDataReader stringDataReader = (StringDataReader) aDataReader;
            final SampleInfo sampleInfo = new SampleInfo();
            String data = stringDataReader.take_next_sample( sampleInfo );
            if ( sampleInfo.valid_data )
            {
                System.out.println( "time : " + data );
            }

        }
    }

    private static class DealDataReaderAdapter
        extends DataReaderAdapter
    {
        @Override
        public void on_data_available( final DataReader aDataReader )
        {
            DealMessageDataReader dealMessageDataReader = (DealMessageDataReader) aDataReader;
            final SampleInfo sampleInfo = new SampleInfo();
            final DealMessage dealMessage = new DealMessage();
            dealMessageDataReader.take_next_sample( dealMessage, sampleInfo );
            if ( sampleInfo.valid_data )
            {
                final Deal deal = new Deal( dealMessage.id, dealMessage.value );
                System.out.println( "deal : " + deal.toString() );
            }

        }
    }
}
