package fr.matoeil.demo.osgi.data.distribution.service.rti.subscriber.internal.activation;

import fr.matoeil.demo.osgi.data.distribution.service.rti.subscriber.internal.RTISubscriber;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class Activator
    implements BundleActivator
{

    RTISubscriber rtiSubscriber_ = new RTISubscriber();

    @Override
    public void start( final BundleContext context )
        throws Exception
    {
        rtiSubscriber_.start();
    }

    @Override
    public void stop( final BundleContext context )
        throws Exception
    {
        rtiSubscriber_.stop();
    }


}
