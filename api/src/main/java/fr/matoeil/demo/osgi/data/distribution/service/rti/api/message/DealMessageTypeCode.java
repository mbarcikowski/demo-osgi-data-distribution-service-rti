
/*
  WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

  This file was generated from .idl using "rtiddsgen".
  The rtiddsgen tool is part of the RTI Connext distribution.
  For more information, type 'rtiddsgen -help' at a command shell
  or consult the RTI Connext manual.
*/
    
package fr.matoeil.demo.osgi.data.distribution.service.rti.api.message;
        
import com.rti.dds.typecode.*;


public class DealMessageTypeCode {
    public static final TypeCode VALUE = getTypeCode();

    private static TypeCode getTypeCode() {
        TypeCode tc = null;
        int i=0;
        StructMember sm[] = new StructMember[2];

        sm[i]=new StructMember("id",false,(short)-1,false,(TypeCode)new TypeCode(TCKind.TK_STRING,255)); i++;
        sm[i]=new StructMember("value",false,(short)-1,false,(TypeCode)TypeCode.TC_LONG); i++;

        tc = TypeCodeFactory.TheTypeCodeFactory.create_struct_tc("fr::matoeil::demo::osgi::data::distribution::service::rti::api::message::DealMessage",ExtensibilityKind.EXTENSIBLE_EXTENSIBILITY,sm);
        return tc;
    }
}
