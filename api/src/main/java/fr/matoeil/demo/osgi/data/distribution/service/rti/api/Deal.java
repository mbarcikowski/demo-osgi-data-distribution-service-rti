package fr.matoeil.demo.osgi.data.distribution.service.rti.api;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class Deal
{
    private final String id_;

    private final int value_;


    public Deal( final String aId, final int aValue )
    {
        id_ = aId;
        value_ = aValue;
    }

    public String getId()
    {
        return id_;
    }

    public int getValue()
    {
        return value_;
    }

    @Override
    public String toString()
    {
        return "Deal{" +
            "id_='" + id_ + '\'' +
            ", value_=" + value_ +
            '}';
    }
}
